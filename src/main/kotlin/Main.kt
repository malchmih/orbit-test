import cloud.orbit.actors.Actor
import cloud.orbit.actors.Stage
import cloud.orbit.actors.cluster.RedisClusterBuilder

fun main() {
    val clusterPeer = RedisClusterBuilder()
        .messagingUri("redis://localhost/")
        .build()

    val stage = Stage.Builder()
        .clusterName("orbit-helloworld-cluster")
        .clusterPeer(clusterPeer)
        .build()
    stage.start().join()
    stage.bind()

    var msg = ""
    while (msg != "exit") {
        msg = readLine().orEmpty()
        val response = Actor.getReference(Hello::class.java, "0").sayHello(msg).join()
        println(response)
    }

    stage.stop().join()
}