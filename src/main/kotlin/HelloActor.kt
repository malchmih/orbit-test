import cloud.orbit.actors.runtime.AbstractActor
import cloud.orbit.concurrent.Task

class HelloActor : AbstractActor<Any>(), Hello {
    override fun sayHello(greeting: String): Task<String> {
        println("Here: $greeting")
        return Task.fromValue("You said: '$greeting', I say: Hello from ${System.identityHashCode(this)} !")
    }
}