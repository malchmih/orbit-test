import cloud.orbit.actors.Actor
import cloud.orbit.concurrent.Task

interface Hello : Actor {
    fun sayHello(greeting: String): Task<String>
}