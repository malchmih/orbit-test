import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    application
    kotlin("jvm") version "1.3.21"
    id("com.github.johnrengelman.shadow") version "4.0.4"
}

group = "orbit"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation("cloud.orbit:orbit-core:1.12.1")
    implementation("cloud.orbit:orbit-runtime:1.12.1")
//    implementation("cloud.orbit:orbit-core:1.13.0-rc2")
//    implementation("cloud.orbit:orbit-runtime:1.13.0-rc2")
//    implementation("cloud.orbit:orbit-redis-cluster:1.4.0-rc2")
    implementation("cloud.orbit:orbit-redis-cluster:1.2.7")
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}

application {
    mainClassName = "MainKt"
}
//
//val jar by tasks.getting(Jar::class) {
//    manifest {
//        attributes["Main-Class"] = "MainKt"
//    }
//}

tasks {
    named<ShadowJar>("shadowJar") {
//        archiveBaseName.set("shadow")
        mergeServiceFiles()
        manifest {
            attributes(mapOf("Main-Class" to "com.github.csolem.gradle.shadow.kotlin.example.App"))
        }
    }
}
